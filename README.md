You need installed ruby & nodejs(npm).

###First time launch:
```bash
gem install compass
npm install
```
####Every time 
When you start working on a project you should update CORE and main files(bower.json, package.json, etc.)
```bash
grunt loadCore
```
> Replace package.json, Gruntfile.js, bower.json, Parameters.js manually if there are some differences(Grunt will notify you about files to be replaced, just read log)
>
> You will find these files in `tmp` directory

After that, type in bash:
```bash
npm install 
bower install
```

For launch server just type in bash/zsh

```bash
grunt serve
```

After these actions you are able to change your code and commit it.

Have a nice day:)