'use strict';

window.AppConfig = {
  title: 'Betforgood',
  sourceIds: {
    live: [2],
    prelive: [1]
  },
  sourcesRelations: {
    1: 2
  },
  betTypes: ['single', 'parlay', 'system'],
  hotCompetitionIds: [],
  // selectCompetitions: 'sport',                       // sport/table/popup options available
  //  competitions: [43],                               // for WinIPL

  defaults: {
    feedType:    'prelive',
    betSlipType: 'single,parlay,system',
    oddViewType: 'decimal',
    lang:        'en',
    currency:    '€'
  },
  urls: {
    api: '//bts-api.dev.bettings.ch/api/v2/',
    wst: '//wst.dev.bettings.ch/api/v1/'             // for tipsters
  },
  updateTime: {
    live:    5000,
    prelive: 5000
  },
  maxCounts: {
    expandMarkets:  5,
    liveEvents:     5,
    upcomingEvents: 20
  },
  sportsIcons: [
    'soccer', 'football', 'basketball', 'tennis', 'volleyball', 'baseball', 'bandy', 'politics',
    'rugby', 'golf', 'darts', 'beach', 'snooker', 'waterpolo', 'boxing', 'curling', 'cricket', 
    'mixed_martial_arts', 'hockey', 'handball', 'motor', 'bowls', 'futsal', 'floorball', 'winter_sports'
  ],
  topMarkets: {
    108: [
      [19,  '1'],
      [786, 'X'],
      [542, '2']
    ],
    384: [
      [57343, 'Over'],
      [65503, 'Under']
    ]
  }
};
